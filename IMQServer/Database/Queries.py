#queries to create table 
CREATE_CLIENT_TABLE = "create table Client ( Client_ID integer PRIMARY KEY, Client_Name varchar(200), Password varchar(50) )"
CREATE_MESSAGE_TABLE = "create table Message( Message_ID integer PRIMARY KEY, Start_Time DATETIME NOT NULL, Data varchar(200)  NOT NULL)"
CREATE_TOPIC_TABLE = "create table Topic(Topic_Name varchar(200) UNIQUE, Queue_ID integer Primary Key)"
CREATE_CLIENT_MESSAGE_MAPPING_TABLE= "create table Client_Message_Mapping( Client_ID integer, Message_ID integer)"
CREATE_MESSAGE_TOPIC_MAPPING = "create table Message_Topic_Mapping( Message_ID integer, Topic_id int"
CREATE_CLIENT_TOPIC_MAPPING = "create table Publisher_Topic_Mapping(Client_ID integer, Topic_id int)"
CREATE_DEAD_LETTER_QUEUE_TABLE = "create table DeadLetterQueue( Message_ID integer PRIMARY KEY, Start_Time DATETIME NOT NULL, Data varchar(200)  NOT NULL)"