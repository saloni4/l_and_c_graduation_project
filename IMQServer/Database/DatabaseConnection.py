import pyodbc

class DatabaseConnection:
    def __int__(self):
        pass

    def openConnection(self):
        try:
            self.conn = pyodbc.connect(
                      'Driver={SQL Server};'
                      'Server=SALONI-ITT;'
                      'Database=imq2;'
                      'Trusted_Connection=yes;')
            self.cursor = self.conn.cursor()
            # print("Connection With DataBase established")
            return self.conn, self.cursor
        except:
            print(" Unable to Establish the Connection to Database.")

    def closeConnection(self):
            print("Closing the connection with Database.")
            self.conn.close()


# db= DatabaseConnection()
# db.openConnection()