#Conatin IMQ Databse OPeration 

import pyodbc
from DatabaseConnection import *
from Queries import *

class ImqDatabaseOperation:
    def __init__(self):
        self.conn_obj=DatabaseConnection()
        self.conn, self.cursor = self.conn_obj.openConnection()

    def createTable(self):
        self.cursor.execute(CREATE_CLIENT_TABLE)
        self.cursor.execute(CREATE_MESSAGE_TABLE)
        self.cursor.execute(CREATE_TOPIC_TABLE)
        self.cursor.execute(CREATE_CLIENT_MESSAGE_MAPPING_TABLE)
        self.cursor.execute(CREATE_MESSAGE_TOPIC_MAPPING)
        self.cursor.execute(CREATE_CLIENT_TOPIC_MAPPING)
        self.cursor.execute(CREATE_DEAD_LETTER_QUEUE_TABLE)
        self.conn.commit()

    def insert(self,client_id, name, password):
        self.cursor.execute("insert into Client VALUES (?, ?, ?)", client_id, name, password)
        self.conn.commit()

    def get(self, id):
        self.cursor.execute('SELECT * FROM Client WHERE Client_ID=?',id)
        # self.cursor.execute('SELECT * FROM Client WHERE Client_ID=134649')
        return self.cursor.fetchone()

    def getSubscriberTopiciFlag(self,clientId, topicId):
        self.cursor.execute('SELECT * FROM Subscriber_Topic_Mapping WHERE Client_iD=? AND topic_Id =? ',clientId ,topicId)
        # self.cursor.execute('SELECT * FROM Client WHERE Client_ID=134649')
        return self.cursor.fetchone()


    def insertMessage(self,message,messageId):
        self.cursor.execute("insert into Message VALUES (?,GETDATE(), ?)",messageId,message)
        self.conn.commit()

    def fetchTopics(self):
        self.cursor.execute('Select * from Topic order by queue_id')
        return self.cursor.fetchall()

    def insertMessageClientMap(self,messageId, clientId):
        self.cursor.execute("insert into Client_Message_Mapping Values ( ?, ?)", clientId, messageId)
        self.conn.commit()

    def insertMessageTopicMap(self, messageId, queueId):
        self.cursor.execute("insert into Message_Topic_Mapping Values ( ?, ?, GETDATE())",messageId, queueId)
        self.conn.commit()

    
    def insertClientTopicMap(self, clientId, topicId):
        self.cursor.execute("insert into Publisher_Topic_Mapping Values ( ?, ?)",clientId, topicId)
        self.conn.commit()

    def insertSubscriberTopicMap(self,clientId, TopicId):
        self.cursor.execute("insert into Subscriber_Topic_Mapping Values ( ?, ?)",clientId, TopicId)
        self.conn.commit()

    def getMessage(self,clientId, topicId):
        self.cursor.execute("SELECT data from Message_Topic_Mapping INNER JOIN Message on Message.Message_ID = Message_Topic_Mapping.message_id where topic_id = ? order by Message.Start_Time desc", topicId)
        return self.cursor.fetchall()

    def deleteMessageAfterExpiration(self):
        self.cursor.execute("Delete from Message where DATEDIFF(day, Start_Time, GETDATE()) >= 2")
        self.conn.commit()

    def getExpiredMessgae(self):
        self.cursor.execute("Select * from Message where DATEDIFF(day, Start_Time, GETDATE()) >= 2")
        return self.cursor.fetchall()

    def insertDeadMessage(self,message):
        self.cursor.execute("insert into DeadLetterQueue VALUES (?,?, ?)",message[0],message[1],message[2])
        self.conn.commit()

    def deleteMessageFromMessageTopicMap(self):
        self.cursor.execute("Delete from Message_Topic_Mapping where DATEDIFF(day, Start_Time, GETDATE()) >= 2")
        self.conn.commit()

    def closeConnection(self):
        self.conn_obj.closeConnection()


# db_obj = ImqDatabaseOperation()
# db_obj.createTable()
# db_obj.closeConnection()