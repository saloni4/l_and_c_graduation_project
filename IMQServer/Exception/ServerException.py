#Contain server related Exception
class ServerException(Exception):
    pass

class SocketNotBind(ServerException):
    def __init__(self):
        self.value = SOCKET_NOT_BIND

class DataSendFail(ServerException):
    def __init__(self):
        self.value = DATA_SEND_FAIL

class ClientNotPresent(ServerException):
    def __init__(self):
        self.value = CLIENT_NOT_PRESENT

class DataReceiveFail(ServerException):
    def __init__(self):
        self.value = DATA_RECEIVE_FAIL

