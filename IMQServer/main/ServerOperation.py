#Perforn Databse Operation from Server 

from Constant import *
import sys
sys.path.append(DATABASE_PATH)
from ImqDatabaseOperation import ImqDatabaseOperation

class ServerOperation:
    def __init__(self):
        self.databaseOperation = ImqDatabaseOperation()

    #function to remove expired message
    def removeMessage(self):
        self.sendMessageToDeadLetterQueue()
        self.databaseOperation.deleteMessageAfterExpiration()
        self.databaseOperation.deleteMessageFromMessageTopicMap()
        print("Expired Messages deleted")

    #function to send expired Message to dead letter queue
    def sendMessageToDeadLetterQueue(self):
        allExpiredMessage = self.databaseOperation.getExpiredMessgae()
        if(allExpiredMessage == None):
            print("No message expired")
        else:
            for message in allExpiredMessage:
                self.databaseOperation.insertDeadMessage(message)
        print("Expired Message sent to dead letter queue")
