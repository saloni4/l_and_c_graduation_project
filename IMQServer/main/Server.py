#All Server operation take place here
import socket, pickle
import _thread
from Constant import *
from ServerOperation import *
import sys
sys.path.append(PROTOCOL_PATH)
from ImqProtocol import *

class Server:
    def __init__(self):
        self.server_socket = socket.socket()
        self.serverOperation = ServerOperation()
        self.imqProtocol = ImqProtocol()
        self.thread_count = 0
        self.clientPort = 0
        try:
            self.server_socket.bind((HOST, PORT))
        except socket.error as e:
            print(str(e))
        print("Waiting for Client")
        self.serverOperation.removeMessage()
        self.server_socket.listen(MAX_CLIENT_NUMBER)

    def client_thread(self, connection, address):
        connection.send(str(address[1]).encode())
        while True:
            data = self.imqProtocol.receive(connection)
            if data == '':
                print( " Closing Connection !!!")
                break
            print(address[1],"---",data)

        connection.close()

    def connection(self):
        while True:
            client, address = self.server_socket.accept()
            print("Connected to " +address[0]+ " " + str(address[1]))
            _thread.start_new_thread(self.client_thread, (client,address))
            self.thread_count += 1
            print(" Thread Count : "+str(self.thread_count))
