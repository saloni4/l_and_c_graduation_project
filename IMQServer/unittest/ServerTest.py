import  unittest
from IMQServer import Server
from ServerOperation import *


class ServerTesting(unittest.TestCase):
    def __init__(self):
        self.server_obj = Server()

    def test_Socket_Creation(self):
        self.assertIsNone(self.server_obj)

    def test_Socket_Binding(self):
        address = self.server_obj.getpeername()
        self.assertEqual(ADDRESS, address)

    def test_accept_connection(self):
        self.assertIsNone(self.server_obj.connection())

    def test_removeMessage(self):
        result = self.ServerOperation.removeMesaage()
        self.assertEqual("Expired messages deleted", result)

    def test_sendMessageToDeadLetterQueue(self)
        result = self.ServerOperation.sendMessageToDeadLetterQueue()
        self.assertEqual("Expired message send to dead letter queue",result)