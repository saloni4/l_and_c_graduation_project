import sys
sys.path.append('D:/l&c/l_and_c_graduation_project/IMQServer/main')
import pickle
from Constant import *

class ImqProtocol:
    def __init__(self):
        self.format = FORMAT
        self.protocolVersion = PROTOCOL_VERSION
        self.destinationURI = HOST
        self.sourceURI = ''
        self.requestType = ''
        self.data = ''

    def SourceUri(self, sourceUri):
        self.sourceURI = sourceUri

    def send(self,message, clientSocket):
        clientSocket.send(pickle.dumps(message))
    
    def receive(self, connection):
        
        data = connection.recv(2048)
        if not data:
            self.data = ''
        else:
            try:
                self.data =  pickle.loads(data)
            except: 
                self.data = "No data Received"
        
        return self.data


