# IMQ System Document
This document provides a brief introduction to each sub-component of IMQ System. In IMQ System we will be having Publishers, Subscribers, Queues, DataBase and a Dead Letter Queue.

## Mechanism

The IMQ system will be implemented using Client-Server Architecture using socket programming.
The Server will support requests from multiple Clients. 
The Clients can either be Publisher or Subscriber.
Only Server will have the Admin Rights such as creating Topics and Queues.

## System Flow

### 1. Publishers

There will be Publishers who will be connecting to Queues with a Topic and send the messages which they would want to Publish in IMQ.

### 2. Subscribers

There will be Subscribers who will be connecting to Queues with a Topic and pull the messages which they would want to process.

####3. Database
There will be a database that will store the message published by the publisher. On request by the subscriber, the messages will be shown to the Subscriber. 

####4. Dead Letter Queue.
After the expiration of messages the messages will be send to dead letter queue and will be removed from the queue. 


