#file to autenticate a client 
from Constant import *
import sys
sys.path.append(DATABASE_PATH )
from ImqDatabaseOperation import *

class VerifyClient:
    def __init__(self, ID, password):
        self.ID = ID
        self.password = password

    def authenticateClient(self):
        databaseOperation = ImqDatabaseOperation()
        result = databaseOperation.get(self.ID)
        if(result == None):
            return False
        elif(result[2] == self.password):
            return True
        else:
            return False
     