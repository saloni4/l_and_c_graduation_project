from django.utils.crypto import get_random_string
from ClientRegistration import Registration
from VerifyClient import VerifyClient
import sys
sys.path.append('D:\l&c\l_and_c_graduation_project\IMQServer\Database')
from ImqDatabaseOperation import ImqDatabaseOperation

class ClientOperation:

    def __init__(self):
        self.databaseOperation = ImqDatabaseOperation()

    def registerClient(self):
        name = input("\nEnter Your name : ")
        password = input("\n Enter the password : ")
        register = Registration()
        clientId = register.addClient(name, password)
        print("\n" +  name + "your user client id is : " + clientId + " . \n")
        print("\n Use Client Id for future Login \n")

    def clientLogin(self):
        try:
            print("\nEnter your Client ID.")
            clientId = int(input())
            print("Enter You Password : ")
            password = input()
        except:
            print("Id not in valid format")
            return False,0
        
        obj = VerifyClient(clientId, password)
        isClient = obj.authenticateClient()
        # print(" Exist : ", isClient)
        return isClient, clientId

    def saveMessage(self, msg, queueId,clientId):
        messageId = get_random_string(3, allowed_chars='123456789')
        self.databaseOperation.insertMessage(msg,messageId)
        self.databaseOperation.insertMessageClientMap(messageId, clientId)
        self.databaseOperation.insertMessageTopicMap(messageId,queueId)
        self.databaseOperation.insertClientTopicMap(clientId, queueId)

    def getTopics(self):
        flag=1
        print('\n------------------------\nTopics-List:')
        topicList = self.databaseOperation.fetchTopics()
        for topic in topicList:
            print(topic[1],'. ',topic[0])
        print("\n---------------------")
        print('\nChoose your Topic')
        try:
            choice = int(input())
        except ValueError:
            print("Invalid Input")
            choice = 0
        for topic in topicList:
            if choice == topic[1]:
                flag=0
                break
        if flag == 0:
            return choice
        else:
            return 0

    def SubscribeInfo(self,clientId, topicChoice):
        result = self.databaseOperation.getSubscriberTopiciFlag(clientId, topicChoice)
        if result == None:
            self.databaseOperation.insertSubscriberTopicMap(clientId, topicChoice)
            return 1
        else:
            return 0


    



