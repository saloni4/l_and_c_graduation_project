import pickle
from Constant import *
import sys
sys.path.append(PROTOCOL_PATH)
sys.path.append(DATABASE_PATH)
from ImqProtocol import *
from ImqDatabaseOperation import ImqDatabaseOperation
from  ClientOperation import *

class Subscriber: 
    def __init__(self):
        self.clientOperation = ClientOperation()
        self.databaseOperation = ImqDatabaseOperation()

#function to choose topic to pull message
    def topicChoice(self,clientId):
        topicChoice = self.clientOperation.getTopics()
        if topicChoice == 0:
            print("Wrong choice made for topic")
        else:
            self.subscribeInfo(clientId,topicChoice)

#choice function for the subscriber 
    def subscribeInfo(self,clientId, topicChoice):
        print("-----------------------------------")
        print("1.Want to Subscribe Now")
        print("2.Have already Subscribed")
        print("-----------------------------------")
        print("Enter your choice")
        choice = int(input())
        flag = self.clientOperation.SubscribeInfo(clientId, topicChoice)
        if choice == 1:
            if flag == 0:
                print("Already Subscribed")
            else:
                print("Subscribed!!Enjoy")
            self.pullMessage(clientId, topicChoice)
        elif choice == 2:
            if flag == 1:
                print('You are not subscribed. You have to subscribe')
            else:
                self.pullMessage(clientId, topicChoice)
        else:
            print("Wrong choice made")

#function to pull message according to subscriber's choice 
    def pullMessage(self,clientId, topicChoice):
        result = self.databaseOperation.getMessage(clientId, topicChoice)
        if result == None:
            print("No Message")
        else:
            print(" Latest Messages are ------------------------")
            for msg,  in result:
                print(msg,'\n')

