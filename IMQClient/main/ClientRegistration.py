#file to register a new client to imq 

from django.utils.crypto import get_random_string
from Constant import *
import sys
sys.path.append(SERVER_PATH)
sys.path.append(DATABASE_PATH)
from ImqDatabaseOperation import ImqDatabaseOperation

class Registration:
    def __init__(self):
        self.client_id = get_random_string(6, allowed_chars='123456789')
        print("Client ID:", self.client_id)

#function to add a new client
    def addClient(self, name, password):
        databaseOperation = ImqDatabaseOperation()
        databaseOperation.insert(self.client_id, name, password)
        return str(self.client_id)
