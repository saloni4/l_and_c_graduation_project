#file to perform publisher operation 

import sys
import pickle
from Constant import *
from ClientOperation import *
sys.path.append(PROTOCOL_PATH)
from ImqProtocol import *

class Publisher:

    def __init__(self,imqProtocol):
        self.clientOperation = ClientOperation()
        self.imqProtocol = imqProtocol

#function to choose topic fromt he topic list 
    def topicChoice(self,clientId, clientSocket):
        choice = self.clientOperation.getTopics()
        if choice == 0:
            print("Wrong choice made for topic")
        else:
            self.sendMessage(clientId, choice,clientSocket)

 #function to push the message as publisher   
    def sendMessage(self, client_id, topicChoice, clientSocket):
        print("Enter your Message :")
        message = input() 
        self.imqProtocol.send(message,clientSocket)
        self.clientOperation.saveMessage(message,topicChoice, client_id)
        try:
            while True:
                choice = input("Want to send more messages(y/Y)? ")
                if choice.lower() == 'y':
                    msg = input("Enter your Message : ")
                    self.imqProtocol.send(msg,clientSocket)
                    self.clientOperation.saveMessage(msg,topicChoice, client_id)
                else:
                    print('EXIT')
                    break
        except KeyboardInterrupt:
            print("Program Terminated by User!")

    