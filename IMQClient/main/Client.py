import socket, pickle
import sys
from Constant import *
sys.path.append(SERVER_PATH)
sys.path.append(PROTOCOL_PATH)
from Publisher import *
from Subscriber import *
from ClientOperation import ClientOperation
from ImqProtocol import *

class Client:
    def __init__(self):
        self.clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientOperation = ClientOperation()
        self.imqProtocol = ImqProtocol()

        
        try:
            self.clientSocket.connect((HOST, PORT))
            print("Welcome to IMQ")
        except socket.error as e:
            exit( "Server not present.")

        response = self.clientSocket.recv(1024)
        self.imqProtocol.SourceUri(response.decode(UTF8))

        self.publisher = Publisher(self.imqProtocol)
        self.subscriber = Subscriber()

    def clientChoiceProgram(self):
        clientChoice = self.getClientChoice()
        if clientChoice == 1:
            self.clientOperation.registerClient()
            isClient, clientId = self.clientOperation.clientLogin()
            return isClient, clientId

        elif clientChoice == 2:
            isClient, clientId  = self.clientOperation.clientLogin()
            if isClient==True:
                print("Client Exists!!!")
                return True, clientId
            elif isClient == False and clientId == 0:
                return False, 0
            elif isClient == False:
                print("Either client id or password is wrong. You have to register!!")
                return False, clientId
        else:
            return False, 0

#function to get client choice for login
    def getClientChoice(self):
        print("\n1. New Client Registration")
        print("2. Old Client Login \n")
        try:
            choice = int(input())
        except ValueError as e:
            print("Invalid Input")
            choice = 0
        return choice

#function to get client role as Publisher or Subscriber 
    def getClientRole(self):
        print("\n1. Publisher")
        print("2. Subscriber \n")
        try:
            choice = int(input())
        except ValueError as e:
            print("Invalid Input")
            choice = 0        
        return choice

#driver function to start 
    def main(self):
        isClientExist, clientId = self.clientChoiceProgram()
        if isClientExist == True:
            role = self.getClientRole()
            if role == 1:
                self.publisher.topicChoice(clientId,self.clientSocket)
            elif role ==2:
                self.subscriber.topicChoice(clientId)
            else:
                print("Wrong role choice")
        else:
            print("Program Terminated by User!")

