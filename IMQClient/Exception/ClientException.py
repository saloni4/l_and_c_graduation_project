#Exception file for client 
class ClientException(Exception):
        pass

class InvalidInput(ClientException):
    def __init__(self):
        self.value = INVALID_INPUT

class ClosingConnection(ClientException):
    def __init__(self):
        self.value =  CLOSING_CONNECTION

class ConnectionError(ClientException):
    def __init__(self):
        self.value = CONNECTION_ERROR

class DataSendError(ClientException):
    def __init__(self):
        self.value = DATA_SEND_ERROR

class DataReceiveError(ClientException):
    def __init__(self):
        self.value = DATA_RECEIVE_ERROR