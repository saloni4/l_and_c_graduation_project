#test file for client 

import unittest
from Constant import *
import sys
sys.path.append(CLIENT_PATH)
from ClientRegistration import *
from ClientOperation import *
from Publisher import *
from Subscriber import *


class ClientTest(unittest.TestCase):
    def test_addClient(self):
        result = ClientOperation.addClient('Saloni','123')
        self.assertEqual('435646', result)

    def test_SubscribeInfo(self):
        result = ClientOperation.SubscribeInfo('435646',1)
        self.assertEqual(1, result)

    def test_saveMessage(self):
        result = ClientOperation.saveMessage('hello',1,'334545')
        self.assertEqual("true",result)
    
    def test_topicChoice(self):
        self.Publisher.topicChoice('123323','45678')
        print("Executed")
    
    def test_sendMessage(self):
        result = self.Publisher.sendMessage('123456',2,'45678'):
        self.assertEqual('Want more',result)

    def test_topicChoice(self):
        result = self.Subscriber.topicChoice('234563')
        self.assertEqual('Wrong choice made', result)

    def test_pullMessage(self):
        result = self.Subscriber.pullMessage('234513', 2)
        self.assertEqual('No Message', result)
